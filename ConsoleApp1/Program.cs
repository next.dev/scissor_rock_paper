﻿using System.Diagnostics.CodeAnalysis;

Console.OutputEncoding = System.Text.Encoding.UTF8;



//codition break!
bool isNumber = false;
int player1Choose = 0;
bool breakPlayer1Input = false;


// require person one input correct (number && 1-3)
do
{
    //call menu instruction
    menu();
    Console.Write("Nguoi choi 1: ");
    //is number ?
    isNumber = int.TryParse(Console.ReadLine(), out player1Choose);

    //update codition break 
    breakPlayer1Input = isNumber && player1Choose > 0 && player1Choose < 4;


    //alert if input breakPlayer1Input wrong 
    if (!breakPlayer1Input)
    {
        Console.WriteLine("vui long nhap  1 hoac 2 hoac 3");
    }


} while (!breakPlayer1Input);


//codition break!
int player2Choose = 0;
bool breakPlayer2Input = false;

Console.WriteLine($"player1 choose : {(scissorRockPaper)player1Choose}");

// require person two input correct (number && 1-3)
do
{
    menu();

    Console.Write("Nguoi choi 2: ");
    //is number ?
    isNumber = int.TryParse(Console.ReadLine(), out player2Choose);

    //update codition break 
    breakPlayer2Input = isNumber && player2Choose > 0 && player2Choose < 4;


    //alert if input breakPlayer2Input wrong 
    if (!breakPlayer2Input)
    {
        Console.WriteLine("vui long nhap  1 hoac 2 hoac 3");
    }


} while (!breakPlayer2Input);

Console.WriteLine($"player1 choose : {(scissorRockPaper)player2Choose}");

//menu

void menu()
{
    Console.WriteLine("tự viết thêm cho nó hay");
    Console.WriteLine("1 is scissor");
    Console.WriteLine("2 is rock");
    Console.WriteLine("3 is paper");
}


// cố gắng viết thêm  vòng lặp để bao giờ người ta muốn ngừng thì ngừng, còn không thì bắt họ chơi đến chết thì thôi
string result = GameResult(player1Choose, player2Choose);
Console.WriteLine(result);


// này nếu em không muốn dùng enum có thể thay số 1-2-3 vào code vẫn hoạt động bình thường
string GameResult(int player1, int player2)
{

    // cast EXPLICIT
    scissorRockPaper playerOne = (scissorRockPaper)player1;

    scissorRockPaper playerTwo = (scissorRockPaper)player2;

    // player1 win ! (all case win of player one)
    bool playerOneWin =
            (playerOne == scissorRockPaper.scissor && playerTwo == scissorRockPaper.paper
        || playerOne == scissorRockPaper.rock && playerTwo == scissorRockPaper.scissor
        || playerOne == scissorRockPaper.paper && playerTwo == scissorRockPaper.rock);

    // draw!
    if (playerOne == playerTwo)
    {
        return "draw!";
    }
    else if (playerOneWin)
    {
        return "player1 win";
    }
    else
    {
        return "player2 win";
    }

}

//không nên để 3 cái const như vậy, nếu cần kiểu dữ liệu liệt kê thì em dùng enum,
enum scissorRockPaper { scissor, rock, paper };

